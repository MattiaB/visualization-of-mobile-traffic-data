/*global require, module,  __dirname, console, process */
/* jslint node: true */

var express = require('express'),
    routes = require('./routes'),
    user = require('./routes/user'),
    http = require('http'),
    path = require('path'),
    app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('json spaces', 0);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.compress());
app.use(express.static(__dirname + '/public'));

app.use(app.router);


// development only
if ('development' === app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);

app.get('/getCellData', routes.getData);
app.get('/getCellDataGroupByDay', routes.getCellDataGroupByDay);
app.get('/getDataWithGeoFilter', routes.getDataWithGeoFilter);

app.get('/getTimestamps', routes.getTimestamps);
app.get('/getMaxActivity', routes.getMaxActivity);





app.get('/users', user.list);

http.createServer(app).listen(app.get('port'), function () {
    "use strict";
    console.log('Express server listening on port ' + app.get('port'));
});
