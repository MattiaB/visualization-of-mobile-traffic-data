/*global require, module,  __dirname, console, process */
/* jslint node: true */

var Q = require('q'),
    pg = require('pg').native,
    _ = require('underscore'),
    connectionString = process.env.DATABASE_URL || 'postgres://mattiab:mattiab@localhost:5432/dataviz',
    client,
    redis = require("redis"),
    redisClient = redis.createClient(),
    controllers,
    getQueryPromise,
    redisGetQueryPromise,
    simpleDataRetrievalAndRender,
    maxCacheAge = 1800000,
    cache = require('memory-cache');
client = new pg.Client(connectionString);
client.connect();

getQueryPromise = function (query, queryData) {
    'use strict';
    var deferred = Q.defer();
    client.query(query, queryData, function (err, result) {
        if (!err) {
            deferred.resolve(result);
        } else {
            deferred.reject(new Error(err));
        }
    });
    return deferred.promise;
};
simpleDataRetrievalAndRender = function (type, res, timestamp, query) {
    'use strict';
    timestamp = new Date(timestamp);
    if (timestamp.toString() === "Invalid Date") {
        res.json({error: 'Request format not allowed', code: 1});
        res.end();
        return Q(undefined);
    }
    return getQueryPromise(query, [timestamp])
        .then(function (queryData) {
            if (queryData && queryData.rows) {
                res.setHeader("Cache-Control:", "max-age=" + maxCacheAge);
                res.json(queryData.rows);
            }
            res.end();
        }).fail(function (err) {
            console.log(err);
            res.status(500);
            res.json({ error: err });
        }).done();
};
redisGetQueryPromise = function (key) {
    'use strict';
    var deferred = Q.defer();
    redisClient.get(key, function (err, reply) {
        if (!err) {
            deferred.resolve(reply);
        } else {
            deferred.reject(new Error(err));
        }
    });
    return deferred.promise;
};

controllers = (function () {
    'use strict';
    return {
        index: function (req, res) {
            res.render('index', { cache: true, filename: 'index'});
        },
        getCellDataGroupByDay: function (req, res) {
            simpleDataRetrievalAndRender('day:', res, req.param('timestamp'),
                'SELECT square as id,  sms_in as s_i, sms_out as s_o, call_in as c_i, call_out as c_o, internet as in ' +
                'FROM "public"."milan_days_data" ' +
                'WHERE milan_days_data.time::date = $1::date ');
        },
        getData: function (req, res) {
            simpleDataRetrievalAndRender('hour:', res, req.param('timestamp'),
                'SELECT square as id,  sms_in as s_i, sms_out as s_o, call_in as c_i, call_out as c_o, internet as in ' +
                    'FROM "public"."milan_hours_data" WHERE time=$1 ');
        },
        getDataWithGeoFilter: function (req, res) {
            var northEastPoint = [], southWestPoint = [], polyline, check, query;
            northEastPoint[0] = parseFloat(req.param('northEastLng'));
            northEastPoint[1] = parseFloat(req.param('northEastLat'));
            southWestPoint[0] = parseFloat(req.param('southWestLng'));
            southWestPoint[1] = parseFloat(req.param('southWestLat'));
            check = _.every(northEastPoint.concat(southWestPoint), function (elem) {
                return !isNaN(elem);
            });
            if (!check) {
                res.json({error: 'Request format not allowed', code: 1});
                res.end();
                return;
            }
            polyline = [[southWestPoint[0], northEastPoint[1]], northEastPoint, [northEastPoint[0], southWestPoint[1]], southWestPoint, [southWestPoint[0], northEastPoint[1]]];
            polyline = _.map(polyline, function (elem) {
                return elem.join(' ');
            }).join(',');
            query = 'SELECT time, ' +
                'MAX(case when sms_in != \'Nan\'::REAL then sms_in end) as max_sms_in, MIN(case when sms_in != \'Nan\'::REAL then sms_in end) as min_sms_in, AVG(case when sms_in != \'Nan\'::REAL then sms_in end) as avg_sms_in, var_pop(case when sms_in != \'Nan\'::REAL then sms_in end) as var_sms_in, ' +
                'MAX(case when sms_out != \'Nan\'::REAL then sms_out end) as max_sms_out, MIN(case when sms_out != \'Nan\'::REAL then sms_out end) as min_sms_out, AVG(case when sms_out != \'Nan\'::REAL then sms_out end) as avg_sms_out, var_pop(case when sms_out != \'Nan\'::REAL then sms_out end) as var_sms_out, ' +
                'MAX(case when call_in != \'Nan\'::REAL then call_in end) as max_call_in, MIN(case when call_in != \'Nan\'::REAL then call_in end) as min_call_in, AVG(case when call_in != \'Nan\'::REAL then call_in end) as avg_call_in, var_pop(case when call_in != \'Nan\'::REAL then call_in end) as var_call_in , ' +
                'MAX(case when call_out != \'Nan\'::REAL then call_out end) as max_call_out, MIN(case when call_out != \'Nan\'::REAL then call_out end) as min_call_out, AVG(case when call_out != \'Nan\'::REAL then call_out end) as avg_call_out, var_pop(case when call_out != \'Nan\'::REAL then call_out end) as var_call_out, ' +
                'MAX(case when internet != \'Nan\'::REAL then internet end) as max_internet, MIN(case when internet != \'Nan\'::REAL then internet end) as min_internet, AVG(case when internet != \'Nan\'::REAL then internet end) as avg_internet, var_pop(case when internet != \'Nan\'::REAL then internet end) as var_internet ' +
                'FROM  "public"."geography_data",  "public"."milan_hours_data" ' +
                'WHERE  ST_Within(geography_data.location::geometry, \'POLYGON((' + polyline + '))\'::geography::geometry) ' +
                'AND geography_data.square = milan_hours_data.square ' +
                'GROUP BY time;';
            getQueryPromise(query, null)
                .then(function (queryData) {
                    if (queryData && queryData.rows) {
                        res.setHeader("Cache-Control:", "max-age=" + maxCacheAge);
                        res.json(queryData.rows);
                    }
                    res.end();
                }).fail(function (err) {
                    console.log(err);
                    res.status(500);
                    res.json({ error: err });
                }).done();
        },
        getTimestamps: function (req, res) {
            var query, resultData;
            resultData = cache.get('timestamps');
            if (resultData) {
                res.setHeader("Cache-Control:", "max-age=" + maxCacheAge);
                res.write(resultData);
                if (!redisClient.exists("timestamps")) {
                    redisClient.set("timestamps", resultData);
                }
                res.end();
                return;
            }
            redisGetQueryPromise("timestamps")
                .then(function (reply) {
                    if (reply) {
                        res.setHeader("Cache-Control:", "max-age=" + maxCacheAge);
                        res.write(reply);
                        cache.put('timestamps', reply);
                        res.end();
                        return '';
                    }
                    query = 'SELECT DISTINCT time FROM "public"."milan_hours_data" ORDER BY time';
                    return getQueryPromise(query, null)
                        .then(function (queryData) {
                            var jsonStringResult;
                            if (queryData && queryData.rows) {
                                res.setHeader("Cache-Control:", "max-age=" + maxCacheAge);
                                res.json(queryData.rows);
                                jsonStringResult = JSON.stringify(queryData.rows);
                                redisClient.set("timestamps", jsonStringResult);
                                cache.put('timestamps', jsonStringResult);
                            }
                            res.end();
                        });

                }).fail(function (err) {
                    console.log(err);
                    res.status(500);
                    res.json({ error: err });
                }).done();

        },
        getMaxActivity: function (req, res) {
            var query, resultData;
            resultData = cache.get('getMaxActivity');
            if (resultData) {
                res.setHeader("Cache-Control:", "max-age=" + maxCacheAge);
                res.write(resultData);
                if (!redisClient.exists("getMaxActivity")) {
                    redisClient.set("getMaxActivity", resultData);
                }
                res.end();
                return;
            }
            redisGetQueryPromise("getMaxActivity")
                .then(function (reply) {
                    if (reply) {
                        res.setHeader("Cache-Control:", "max-age=" + maxCacheAge);
                        res.write(reply);
                        cache.put('getMaxActivity', reply);
                        res.end();
                        return '';
                    }
                    query = 'select count(time) as tot_data, ' +
                        'MAX(case when sms_in != \'Nan\'::REAL then sms_in end) as max_sms_in, MIN(case when sms_in != \'Nan\'::REAL then sms_in end) as min_sms_in, AVG(case when sms_in != \'Nan\'::REAL then sms_in end) as avg_sms_in, var_pop(case when sms_in != \'Nan\'::REAL then sms_in end) as var_sms_in, ' +
                        'MAX(case when sms_out != \'Nan\'::REAL then sms_out end) as max_sms_out, MIN(case when sms_out != \'Nan\'::REAL then sms_out end) as min_sms_out, AVG(case when sms_out != \'Nan\'::REAL then sms_out end) as avg_sms_out, var_pop(case when sms_out != \'Nan\'::REAL then sms_out end) as var_sms_out, ' +
                        'MAX(case when call_in != \'Nan\'::REAL then call_in end) as max_call_in, MIN(case when call_in != \'Nan\'::REAL then call_in end) as min_call_in, AVG(case when call_in != \'Nan\'::REAL then call_in end) as avg_call_in, var_pop(case when call_in != \'Nan\'::REAL then call_in end) as var_call_in ,' +
                        'MAX(case when call_out != \'Nan\'::REAL then call_out end) as max_call_out, MIN(case when call_out != \'Nan\'::REAL then call_out end) as min_call_out, AVG(case when call_out != \'Nan\'::REAL then call_out end) as avg_call_out, var_pop(case when call_out != \'Nan\'::REAL then call_out end) as var_call_out, ' +
                        'MAX(case when internet != \'Nan\'::REAL then internet end) as max_internet, MIN(case when internet != \'Nan\'::REAL then internet end) as min_internet, AVG(case when internet != \'Nan\'::REAL then internet end) as avg_internet, var_pop(case when internet != \'Nan\'::REAL then internet end) as var_internet ' +
                        'FROM "public"."milan_hours_data" ';
                    return getQueryPromise(query, null)
                        .then(function (queryData) {
                            if (queryData && queryData.rows) {
                                var jsonStringResult;
                                res.setHeader("Cache-Control:", "max-age=" + maxCacheAge);
                                res.json(queryData.rows);
                                jsonStringResult = JSON.stringify(queryData.rows);
                                redisClient.set("getMaxActivity", jsonStringResult);
                                cache.put('getMaxActivity', jsonStringResult);
                            }
                            res.end();
                        });

                }).fail(function (err) {
                    console.log(err);
                    res.status(500);
                    res.json({ error: err });
                }).done();
        }
    };
}());
module.exports = controllers;