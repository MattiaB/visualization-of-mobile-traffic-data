import os

__author__ = 'Mattia Bertorello'

import glob
import json
import numpy
import re
import psycopg2
import datetime
import time
import pandas as pd
import signal
import sys

backup = {'finished': [], 'begin': ''}


def signal_handler(signal, frame):
    print 'You pressed Ctrl+C!'
    f = open('backup.json', 'w')
    json.dump(backup, f)
    f.close()
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)


class Models:
    total_insert = 0

    def __init__(self, table_name_param):
        self.conn = psycopg2.connect(database="dataviz", user="mattiab", password="mattiab", host='localhost')
        self.cur = self.conn.cursor()
        self.count = 0
        self.cache = []
        self.start_time_between_two_query = time.time()
        self.table_name = table_name_param

    def insert(self, data_query):
        if isinstance(data_query, list):
            self.cache.extend(data_query)
        else:
            self.cache.append(tuple(data_query))
        if len(self.cache) > 10000:
            Models.total_insert += len(self.cache)
            print(Models.total_insert)
            self._exec_query()
            self.conn.commit()

    def _exec_query(self):
        print 'Time between one query and another: ' + str(time.time() - self.start_time_between_two_query)
        self.start_time_between_two_query = time.time()
        start = time.time()
        self.cur.executemany("INSERT INTO  \"public\".\"" + self.table_name + """" (square, time, sms_in,
                sms_out, call_in, call_out,
                internet, sms_in_variance, sms_out_variance, call_in_variance,
                call_out_variance, internet_variance)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);""", self.cache)
        self.cache = []
        end = time.time()
        print 'Query time: ' + str(end - start)

    def delete_all_with_date(self, date):
        self.cur.execute("DELETE FROM \"public\".\"" + self.table_name + """"
        WHERE EXTRACT(DAY FROM  time) = EXTRACT(DAY FROM TIMESTAMP %s) and
        EXTRACT(MONTH FROM  time) = EXTRACT(MONTH FROM TIMESTAMP %s) and
        EXTRACT(YEAR FROM  time) = EXTRACT(YEAR FROM TIMESTAMP %s);""", [date, date, date])
        self.conn.commit()

    def close(self):
        if len(self.cache) > 0:
            self._exec_query()
            self.conn.commit()
        print(self.total_insert)
        self.cur.close()
        self.conn.close()


class MockModels:
    def __init__(self):
        pass

    def insert(self, data_query):
        pass

    def exec_query(self):
        pass

    def close(self):
        pass


class CacheData:
    def __init__(self):
        self.datetime_obj = datetime.datetime(2014, 3, 6)
        self.data_cache_dict = {}

    def group_data(self, row):
        timestamp = self.datetime_obj.fromtimestamp(row[1] / 1000)
        timestamp = timestamp.replace(minute=0)
        hour_list_value = self.data_cache_dict.get(timestamp, [])
        hour_list_value.append(row)
        self.data_cache_dict[timestamp] = hour_list_value

    def get_cache_data(self):
        total_results = []
        for key, value in self.data_cache_dict.iteritems():
            row = zip(*value)
            row_results = [0] * 7
            row_results_var = [0] * 5
            for i in xrange(2, 7):
                values = [x for x in row[i] if not numpy.isnan(x)]
                row_results[i] = numpy.mean(values)
                row_results_var[i - 2] = numpy.var(values)
            row_results[0] = row[0][0]
            row_results[1] = key
            row_results.extend(row_results_var)
            #row_results[8] = test[0][0]
            total_results.append(row_results)

        self.data_cache_dict = {}
        return total_results


def read_back_up_data(backup_filename):
    global backup
    result = [None] * 2
    if os.path.exists(backup_filename):
        f = file(backup_filename, "r")
    else:
        f = file(backup_filename, "w")
    try:
        backup = json.load(f)
    except:
        pass
    finally:
        f.close()
    result[0] = backup.get('finished', [])
    if backup.get('begin', '') != '':
        m = re.search('(\d{4})-(\d{2})-(\d{2})\.txt', backup.get('begin', ''))
        result[1] = datetime.datetime(int(m.group(1)), int(m.group(2)), int(m.group(3)))
    return result


def importer(pandas_data_frame, database_obj):
    actual_square_id = 0
    cache_data_square = CacheData()
    backup['begin'] = pandas_data_frame.f
    for chunk in pandas_data_frame:
        for i in xrange(0, len(chunk)):
            if chunk['country_code'][i] != 39:
                continue

            square_id = chunk['square'][i]
            if actual_square_id != square_id and actual_square_id != 0:
                database_obj.insert(
                    cache_data_square.get_cache_data()
                )
                actual_square_id = square_id

            if actual_square_id == 0:
                actual_square_id = square_id

            if actual_square_id == square_id:
                cache_data_square.group_data([square_id, chunk['time'][i], chunk['sms_in'][i],
                                              chunk['sms_out'][i], chunk['call_in'][i],
                                              chunk['call_out'][i], chunk['internet'][i]])
    backup['begin'] = ''
    database_obj.insert(cache_data_square.get_cache_data())


if __name__ == '__main__':
    array_of_file_names = glob.glob(sys.argv[1])
    database = Models(sys.argv[2])
    finished, day_to_delete = read_back_up_data('backup.json')
    array_of_file_names = filter(lambda x: not x in finished, array_of_file_names)
    if day_to_delete:
        database.delete_all_with_date(day_to_delete)

    pandas_data_frames = [pd.read_csv(filepath_or_buffer=filename,
                                      header=None, delimiter='\t',
                                      iterator=True, index_col=False,
                                      chunksize=100000,
                                      names=['square', 'time', 'country_code',
                                             'sms_in', 'sms_out', 'call_in',
                                             'call_out', 'internet'],
                                      dtype={'square': int, 'country_code': numpy.int16, 'sms_in': numpy.float64,
                                             'time': numpy.int64,'call_in': numpy.float64,
                                             'call_out': numpy.float64, 'sms_out': numpy.float64,
                                             'internet': numpy.float64}) for filename in array_of_file_names]

    for pandas_data_frame in pandas_data_frames:
        print pandas_data_frame.f
        start = time.time()
        importer(pandas_data_frame, database)
        backup['finished'].append(pandas_data_frame.f)
        end = time.time()
        print 'Time to upload a file: ' + str(end - start)

    database.close()
    f = open('backup.json', 'w')
    json.dump(backup, f)
    f.close()
