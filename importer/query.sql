CREATE TABLE milan_hours_data
(
    square SMALLINT NOT NULL,
    time TIMESTAMP NOT NULL,
    sms_in REAL NULL,
    sms_in_variance REAL NULL,
    sms_out REAL NULL,
    sms_out_variance REAL NULL,
    call_in REAL NULL,
    call_in_variance REAL  NULL,
    call_out REAL NULL,
    call_out_variance REAL NULL,
    internet REAL NULL,
    internet_variance REAL NULL
);
CREATE TABLE geography_data
(
    square SMALLINT NOT NULL,
    location GEOGRAPHY(Polygon) NOT NULL
);
ALTER TABLE public.milan_hours_data ADD PRIMARY KEY ( square, time );
CREATE INDEX milan_hours_data_time_index ON milan_hours_data ( "time" );
CREATE INDEX milan_hours_data_square_index ON milan_hours_data ( square );
CREATE INDEX geography_data_location_index ON geography_data USING gist(location);

CREATE TABLE milan_days_data
(
    square SMALLINT NOT NULL,
    time TIMESTAMP NOT NULL,
    sms_in REAL NULL ,
    sms_in_variance REAL NULL ,
    sms_out REAL NULL ,
    sms_out_variance REAL NULL ,
    call_in REAL NULL ,
    call_in_variance REAL NULL ,
    call_out REAL NULL ,
    call_out_variance REAL NULL ,
    internet REAL NULL ,
    internet_variance REAL NULL
);

ALTER TABLE public.milan_days_data ADD PRIMARY KEY ( square, time );
CREATE INDEX milan_days_data_time_index ON milan_days_data ( "time" );
CREATE INDEX milan_days_data_square_index ON milan_days_data ( square );

INSERT INTO  "public"."milan_days_data"
                (square, time, sms_in,sms_in_variance,
                sms_out, sms_out_variance, call_in, call_in_variance, call_out, call_out_variance,
                internet, internet_variance) (
  SELECT square as square,
  time::date as time,
  avg(case when sms_in != 'Nan'::REAL then sms_in end)::REAL as sms_in,
  var_pop(case when sms_in != 'Nan'::REAL then sms_in end)::REAL as sms_in_variance,
  avg(case when sms_out != 'Nan'::REAL then sms_out end)::REAL as sms_out,
  var_pop(case when sms_out != 'Nan'::REAL then sms_out end)::REAL as sms_out_variance,
  avg(case when call_in != 'Nan'::REAL then call_in end)::REAL as call_in,
  var_pop(case when call_in != 'Nan'::REAL then call_in end)::REAL as call_in_variance,
  avg(case when call_out != 'Nan'::REAL then call_out end)::REAL as call_out,
  var_pop(case when call_out != 'Nan'::REAL then call_out end)::REAL as call_out_variance,
  avg(case when internet != 'Nan'::REAL then internet end)::REAL as internet,
  var_pop(case when internet != 'Nan'::REAL then internet end)::REAL as internet_variance
FROM "public"."milan_hours_data"
GROUP BY square, time::date
);

SELECT DISTINCT time FROM "public"."milan_hours_data" ORDER BY time;

select count(time) as tot_data,
  MAX(case when sms_in != 'Nan'::REAL then sms_in end) as max_sms_in, MIN(case when sms_in != 'Nan'::REAL then sms_in end) as min_sms_in, AVG(case when sms_in != 'Nan'::REAL then sms_in end) as avg_sms_in, var_pop(case when sms_in != 'Nan'::REAL then sms_in end) as var_sms_in, 
  MAX(case when sms_out != 'Nan'::REAL then sms_out end) as max_sms_out, MIN(case when sms_out != 'Nan'::REAL then sms_out end) as min_sms_out, AVG(case when sms_out != 'Nan'::REAL then sms_out end) as avg_sms_out, var_pop(case when sms_out != 'Nan'::REAL then sms_out end) as var_sms_out, 
  MAX(case when call_in != 'Nan'::REAL then call_in end) as max_call_in, MIN(case when call_in != 'Nan'::REAL then call_in end) as min_call_in, AVG(case when call_in != 'Nan'::REAL then call_in end) as avg_call_in, var_pop(case when call_in != 'Nan'::REAL then call_in end) as var_call_in ,
  MAX(case when call_out != 'Nan'::REAL then call_out end) as max_call_out, MIN(case when call_out != 'Nan'::REAL then call_out end) as min_call_out, AVG(case when call_out != 'Nan'::REAL then call_out end) as avg_call_out, var_pop(case when call_out != 'Nan'::REAL then call_out end) as var_call_out, 
  MAX(case when internet != 'Nan'::REAL then internet end) as max_internet, MIN(case when internet != 'Nan'::REAL then internet end) as min_internet, AVG(case when internet != 'Nan'::REAL then internet end) as avg_internet, var_pop(case when internet != 'Nan'::REAL then internet end) as var_internet 
FROM "public"."milan_hours_data";

SELECT time,
  MAX(case when sms_in != 'Nan'::REAL then sms_in end) as max_sms_in, MIN(case when sms_in != 'Nan'::REAL then sms_in end) as min_sms_in, AVG(case when sms_in != 'Nan'::REAL then sms_in end) as avg_sms_in, var_pop(case when sms_in != 'Nan'::REAL then sms_in end) as var_sms_in,
  MAX(case when sms_out != 'Nan'::REAL then sms_out end) as max_sms_out, MIN(case when sms_out != 'Nan'::REAL then sms_out end) as min_sms_out, AVG(case when sms_out != 'Nan'::REAL then sms_out end) as avg_sms_out, var_pop(case when sms_out != 'Nan'::REAL then sms_out end) as var_sms_out,
  MAX(case when call_in != 'Nan'::REAL then call_in end) as max_call_in, MIN(case when call_in != 'Nan'::REAL then call_in end) as min_call_in, AVG(case when call_in != 'Nan'::REAL then call_in end) as avg_call_in, var_pop(case when call_in != 'Nan'::REAL then call_in end) as var_call_in ,
  MAX(case when call_out != 'Nan'::REAL then call_out end) as max_call_out, MIN(case when call_out != 'Nan'::REAL then call_out end) as min_call_out, AVG(case when call_out != 'Nan'::REAL then call_out end) as avg_call_out, var_pop(case when call_out != 'Nan'::REAL then call_out end) as var_call_out,
  MAX(case when internet != 'Nan'::REAL then internet end) as max_internet, MIN(case when internet != 'Nan'::REAL then internet end) as min_internet, AVG(case when internet != 'Nan'::REAL then internet end) as avg_internet, var_pop(case when internet != 'Nan'::REAL then internet end) as var_internet
FROM  "public"."milan_hours_data", "public"."geography_data"
WHERE  st_contains(geography_data.location::geometry,
                       'POLYGON((9.0114910478323 45.3588013144097,9.01449148801314 45.358800973144,9.0144909480813 45.3566856534149,9.01149061969251 45.3566859946555,9.0114910478323 45.3588013144097))'::geography::geometry)
      AND geography_data.square = milan_hours_data.square
GROUP BY time
  limit 10;

  CREATE TABLE trento_hours_data
(
    square SMALLINT NOT NULL,
    time TIMESTAMP NOT NULL,
    sms_in REAL NULL,
    sms_in_variance REAL NULL,
    sms_out REAL NULL,
    sms_out_variance REAL NULL,
    call_in REAL NULL,
    call_in_variance REAL  NULL,
    call_out REAL NULL,
    call_out_variance REAL NULL,
    internet REAL NULL,
    internet_variance REAL NULL
);
CREATE TABLE trento_geography_data
(
    square SMALLINT NOT NULL,
    location GEOGRAPHY(Polygon) NOT NULL
);
ALTER TABLE public.trento_hours_data ADD PRIMARY KEY ( square, time );
CREATE INDEX trento_hours_data_time_index ON trento_hours_data ( "time" );
CREATE INDEX trento_hours_data_square_index ON trento_hours_data ( square );
CREATE INDEX trento_geography_data_location_index ON trento_geography_data USING gist(location);


CREATE TABLE trento_days_data
(
    square SMALLINT NOT NULL,
    time TIMESTAMP NOT NULL,
    sms_in REAL NULL ,
    sms_in_variance REAL NULL ,
    sms_out REAL NULL ,
    sms_out_variance REAL NULL ,
    call_in REAL NULL ,
    call_in_variance REAL NULL ,
    call_out REAL NULL ,
    call_out_variance REAL NULL ,
    internet REAL NULL ,
    internet_variance REAL NULL
);

ALTER TABLE public.trento_days_data ADD PRIMARY KEY ( square, time );
CREATE INDEX trento_days_data_time_index ON trento_days_data ( "time" );
CREATE INDEX trento_days_data_square_index ON trento_days_data ( square );

INSERT INTO  "public"."trento_days_data"
                (square, time, sms_in,sms_in_variance,
                sms_out, sms_out_variance, call_in, call_in_variance, call_out, call_out_variance,
                internet, internet_variance) (
  SELECT square as square,
  time::date as time,
  avg(case when sms_in != 'Nan'::REAL then sms_in end)::REAL as sms_in,
  var_pop(case when sms_in != 'Nan'::REAL then sms_in end)::REAL as sms_in_variance,
  avg(case when sms_out != 'Nan'::REAL then sms_out end)::REAL as sms_out,
  var_pop(case when sms_out != 'Nan'::REAL then sms_out end)::REAL as sms_out_variance,
  avg(case when call_in != 'Nan'::REAL then call_in end)::REAL as call_in,
  var_pop(case when call_in != 'Nan'::REAL then call_in end)::REAL as call_in_variance,
  avg(case when call_out != 'Nan'::REAL then call_out end)::REAL as call_out,
  var_pop(case when call_out != 'Nan'::REAL then call_out end)::REAL as call_out_variance,
  avg(case when internet != 'Nan'::REAL then internet end)::REAL as internet,
  var_pop(case when internet != 'Nan'::REAL then internet end)::REAL as internet_variance
FROM "public"."trento_hours_data"
GROUP BY square, time::date
);