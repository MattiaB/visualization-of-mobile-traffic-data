import json

__author__ = 'mattia'

import psycopg2
import time
import sys


class Grid:
    def __init__(self, path, database_param):
        f = open(path, 'r')
        self.geo_grid_dict = {}
        self.database = database_param
        self.initial(json.loads(f.readline()))

    @staticmethod
    def convert_coordinates(coordinates):
        result_list = []
        for coordinate in coordinates[0]:
            coordinate = [repr(x) for x in coordinate]
            result_list.append(' '.join(coordinate))
        return "POLYGON((" + ','.join(result_list) + "))"

    def initial(self, geo_grid):
        for feature in geo_grid['features']:
            self.database.insert([[feature['properties']['cellId'],
                                   Grid.convert_coordinates(feature['geometry']['coordinates'])]])

    def get_postgis_polygon(self, square_id):
        return self.geo_grid_dict[square_id]


class Models:
    total_insert = 0

    def __init__(self, table_name_param):
        self.conn = psycopg2.connect(database="dataviz", user="mattiab", password="mattiab", host='localhost')
        self.cur = self.conn.cursor()
        self.count = 0
        self.cache = []
        self.start_time_between_two_query = time.time()
        self.table_name = table_name_param

    def insert(self, data_query):
        if isinstance(data_query, list):
            self.cache.extend(data_query)
        else:
            self.cache.append(tuple(data_query))
        if len(self.cache) > 5000:
            Models.total_insert += len(self.cache)
            print(Models.total_insert)
            self._exec_query()
            self.conn.commit()

    def _exec_query(self):
        print 'Time between one query and another: ' + str(time.time() - self.start_time_between_two_query)
        self.start_time_between_two_query = time.time()
        start = time.time()
        self.cur.executemany("INSERT INTO  \"public\".\"" + self.table_name + '" ' +
                """(square, location)
                VALUES (%s, ST_GeomFromText(%s));""", self.cache)
        self.cache = []
        end = time.time()
        print 'Query time: ' + str(end - start)

    def close(self):
        if len(self.cache) > 0:
            self._exec_query()
            Models.total_insert += len(self.cache)
            self.conn.commit()
        print(self.total_insert)
        self.cur.close()
        self.conn.close()


class MockModels:
    def __init__(self):
        pass

    def insert(self, data_query):
        pass

    def exec_query(self):
        pass

    def close(self):
        pass


if __name__ == '__main__':
    database = Models(sys.argv[2])
    grid = Grid(sys.argv[1], database)
    database.close()
