/*global define:false, $:false, console:false, setTimeout:false, document, google*/

define(
    ['app/datamodel', 'app/googleMap', 'jquery', 'moment', 'underscore', 'dot', 'jquery_ui'],
    function (model, googleMaps, $, moment, _, doT) {
        "use strict";
        var timeSlider = $("#time-slider"),
            timeText = $('#time-text-panel'),
            activitySlideText = $('#activity-slide-text'),
            activityRelativeSlideText = $('#relative-activity-slide-text'),
            relativeActivitySlider = $("#relative-activity-slider"),
            activityRangeSlider = $('#activity-range-slider'),
            dateFormatter,
            initializeActivityFilterButton,
            updateViewFromStatus,
            updateActivitySliderText,
            updateRelativeActivitySliderText,
            run,
            templateGeneralData = doT.template(document.getElementById("general-statistic-template").text);
        dateFormatter = function (textDate) {
            var groupBy = model.status.group_by;
            if (groupBy === 'hour') {
                return moment(new Date(textDate))
                    .format('dddd, MMMM Do YYYY, H:mm');
            }
            if (groupBy === 'day') {
                return moment(new Date(textDate))
                    .format('dddd, MMMM Do YYYY');
            }
            return '';
        };
        timeSlider.slider({
            max: 0,
            min: 0,
            value: 0,
            change: function (event, ui) {
                //console.log("change" + ui.value);
                model.setStatus('current_timestamps', model.getTimestampsArray(ui.value));
                model.update().then(function () {
                    googleMaps.updateView();
                });
            },
            slide: function (event, ui) {
                var date;
                //console.log("slide" + ui.value);
                if (model.getTimestampsArray()) {
                    timeText.text(
                        dateFormatter(model.getTimestampsArray(ui.value))
                    );
                }
            }
        });
        model.getTimestamps.then(function (timestamps) {
            timeSlider.slider("option", "max", timestamps.length - 1);
            timeText.text(
                dateFormatter(model.getTimestampsArray(0))
            );
            timeSlider.slider("option", "value", 0);
        });
        $('#delete-rectangle-button').button();
        $('#delete-rectangle-button').click(function () {
            googleMaps.deleteShape();
        });
        updateActivitySliderText = function (values) {
            var activityValues = model.getActivityValues(values);
            if (activityValues) {
                activitySlideText.text(activityValues[0].toFixed(2) + ' - ' + activityValues[1].toFixed(2));
            } else {
                activitySlideText.text('');
            }
        };
        updateRelativeActivitySliderText = function (value) {
            var activityRelativeValues = model.getActivityRelativeValue(value);
            if (activityRelativeValues) {
                activityRelativeSlideText.text(activityRelativeValues.toFixed(2));
            } else {
                activityRelativeSlideText.text('');
            }
        };
        updateViewFromStatus = function () {
            var propType = 'checked';
            $('#sms-in-check').prop(propType, model.status.sms_in);
            $('#sms-out-check').prop(propType, model.status.sms_out);
            $('#call-in-check').prop(propType, model.status.call_in);
            $('#call-out-check').prop(propType, model.status.call_out);
            $('#internet-check').prop(propType, model.status.internet);
            $('#group-time').prop('value', model.status.group_by);
            $('#activity-sms-type-selector').buttonset('refresh');
            $('#activity-call-type-selector').buttonset('refresh');
            $("#activity-internet-type-button").buttonset('refresh');
            $('#activity-log-scale-button').buttonset('refresh');
            relativeActivitySlider.slider('option', 'value', model.status.activity_relative_value);
            relativeActivitySlider.slider('option', 'max', model.status.activity_max_relative_value);
            activityRangeSlider.slider('option', 'values', model.status.activity_value_range);
            activityRangeSlider.slider('option', 'max', model.status.activity_max_value);
            if (model.getTimestampsArray()) {
                timeSlider.slider("option", "max", model.getTimestampsArray().length - 1);
                if (timeSlider.slider("option", "value") > (model.getTimestampsArray().length - 1)) {
                    timeSlider.slider("option", "value", model.getTimestampsArray().length - 1);
                }
            }
            updateActivitySliderText();
            updateRelativeActivitySliderText();
            if (model.getTimestampsArray()) {
                timeText.text(
                    dateFormatter(model.getTimestampsArray([timeSlider.slider("option", "value")]))
                );
            }
        };
        initializeActivityFilterButton = function () {
            var changeStatus = function (propType, key, rule) {
                    return function () {
                        model.setStatus(key, $(this).prop(propType));
                        if (rule) {
                            if (rule === 'no-internet') {
                                model.setStatus('internet', false);
                            }
                            if (rule === 'no-sms-call') {
                                _.each(['sms_in', 'sms_out', 'call_in', 'call_out'], function (elem) {
                                    model.setStatus(elem, false);
                                });
                            }
                        }
                        updateViewFromStatus();
                        model.update().then(googleMaps.updateView);
                    };
                },
                changeStatusKurry = _.partial(changeStatus, 'checked');
            $('#sms-in-check').click(changeStatusKurry('sms_in', 'no-internet'));
            $('#sms-out-check').click(changeStatusKurry('sms_out', 'no-internet'));
            $('#call-in-check').click(changeStatusKurry('call_in', 'no-internet'));
            $('#call-out-check').click(changeStatusKurry('call_out', 'no-internet'));
            $('#internet-check').click(changeStatusKurry('internet', 'no-sms-call'));
            $('#activity-low-scale-checkbox').click(changeStatusKurry('low_scale'));
            $('#group-time').change(changeStatus('value', 'group_by'));

        };
        initializeActivityFilterButton();

        $('.button-info').button({
            text: false,
            icons: {
                primary: "ui-icon-info"
            }
        });
        activityRangeSlider.slider({
            min: 0,
            range: true,
            change: function (event, ui) {
                //console.log("change" + ui.values);
                model.setStatus('activity_value_range', ui.values);
                updateActivitySliderText();
                model.update().then(googleMaps.updateView);
            },
            slide: function (event, ui) {
                //console.log("slide" + ui.values);
                updateActivitySliderText(ui.values);
            }
        });
        relativeActivitySlider.slider({
            min: 0,
            change: function (event, ui) {
                //console.log("change" + ui.value);
                model.setStatus('activity_relative_value', ui.value);
                updateRelativeActivitySliderText();
                model.update().then(googleMaps.updateView);
            },
            slide: function (event, ui) {
                //console.log("slide" + ui.value);
                updateRelativeActivitySliderText(ui.value);
            }
        });
        model.getMaxActivityPromise.then(function (data) {
            var statistic = model.status.statistic_values;
            $('#show-statistic-general-data').html(templateGeneralData(statistic));
            updateViewFromStatus();
        });
        model.getDataPromise().then(function () {
            googleMaps.updateView();
        });
        $('#activity-sms-type-selector').buttonset();
        $('#activity-call-type-selector').buttonset();
        $("#activity-internet-type-button").buttonset();
        $('#activity-low-scale-button').buttonset();
        $("#time-play").button({
            text: false,
            icons: {
                primary: "ui-icon-play"
            }
        });
        $('#time-play').click(function () {
            var options, i, simulation, cacheIndex, start;
            if (!run) {
                i = timeSlider.slider("option", "value") + 1;
                cacheIndex = i;
                options = {
                    label: "pause",
                    icons: {
                        primary: "ui-icon-pause"
                    }
                };
                _.each(_.range(cacheIndex, cacheIndex + 3), function (index) {
                    model.cacheData(model.getTimestampsArray(index));
                });
                cacheIndex += 3;
                start = new Date();
                simulation = function () {
                    model.update().then(function () {
                        if (run) {
                            var end = new Date();
                            console.log((end - start) / 1000);
                            googleMaps.updateView();
                            timeSlider.slider("option", "value", i);
                            model.setStatus('current_timestamps', model.getTimestampsArray(i));
                            updateViewFromStatus();
                            i += 1;
                            if (cacheIndex - i > 5) {
                                _.each(_.range(cacheIndex, cacheIndex + 2), function (index) {
                                    model.cacheData(model.getTimestampsArray(index));
                                });
                                cacheIndex += 2;
                            }
                            if (run && i < model.getTimestampsArray().length) {
                                start = new Date();
                                _.delay(simulation, 1000);
                            }
                        }
                    }, function (err) {
                        console.log(err);
                    });
                };
                run = true;
                _.defer(simulation);
            } else {
                options = {
                    label: "play",
                    icons: {
                        primary: "ui-icon-play"
                    }
                };
                run = false;
            }
            $(this).button("option", options);
        });
        // Help
        $('#activity-range-button-info').click(function () {
            var data = model.getAllCurrentData(), sortDataFunc, meanData, mean, drawChart, tableData, options, chart, squareIds, finalData, prevLength;
            prevLength = data.length;
            data = _.filter(data, function (elem, index) {
                return index % 20 === 0;
            });
            sortDataFunc = function (key) {
                return (_.map(data, function (elem) {
                    return elem[key];
                })).sort(
                    function (a, b) {
                        return b - a;
                    }
                );
            };
            squareIds = _.filter(_.range(1, prevLength), function (elem, index) {
                return index % 20 === 0;
            });
            finalData = [['x', 'SMS in', 'SMS. out', 'Call in', 'Call out']].concat(
                _.zip(squareIds, sortDataFunc('s_i'), sortDataFunc('s_o'),
                    sortDataFunc('c_i'), sortDataFunc('c_o'))
            );
            tableData = google.visualization.arrayToDataTable(finalData);
            options = {
                title: '',
                vAxis: {logScale: true},
                curveType: 'function',
                width: 500,
                height: 600
            };
            chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            chart.draw(tableData, options);
        });
        return {
            initializeEvent: function () {
                updateViewFromStatus();
                $('#controller-container').removeClass('hidden');
                return null;
            },
            updateView: updateViewFromStatus
        };
    }
);