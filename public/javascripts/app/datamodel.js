/*global define:false, _:false, $:false, console:false */

define(
    ['underscore', 'jquery', 'q'],
    function (_, $, Q) {
        "use strict";
        var status = {
            group_by: 'hour',
            sms_in: true,
            sms_out: true,
            call_in: true,
            call_out: true,
            internet: false,
            current_timestamps: null,
            activity_relative_value: 50,
            activity_max_relative_value: 500,
            activity_value_range: [1, 500],
            activity_max_value: 250,
            statistic_values: {},
            data_timestamps: {},
            squares_geometry: {},
            low_scale: false,
            data_timestamps_cache_promise: {}
        }, dataModelObject,
            getTimestampsPromise = (function () {
                var deferred = Q.defer();
                $.getJSON('/getTimestamps', deferred.resolve);
                return deferred.promise;
            }()),
            getMaxActivityPromise = (function () {
                var deferred = Q.defer();
                $.getJSON('/getMaxActivity', deferred.resolve);
                return deferred.promise;
            }()),
            squareDataPromise = (function () {
                var deferred = Q.defer();
                $.getJSON('/milano-grid.geojson.js', deferred.resolve);
                return deferred.promise;
            }()),
            getSquaresData = function () {
                var deferred = Q.defer();
                $.getJSON('/getCellData',  {
                    timestamp: status.current_timestamps
                }, deferred.resolve);
                return deferred.promise;
            },
            getSquaresDataGroupByDay = function () {
                var deferred = Q.defer();
                $.getJSON('/getCellDataGroupByDay',  {
                    timestamp: status.current_timestamps
                }, deferred.resolve);
                return deferred.promise;
            },
            timestampSetPromise, maxActivitySetPromise, dataPromise, setDataTimestamps;
        timestampSetPromise = getTimestampsPromise.then(function (data) {
            status.timestamps = _.map(data, function (timestamp) {
                return new Date(timestamp.time);
            });
            status.timestamps_days = _.map(status.timestamps, function (timestamp) {
                return new Date(timestamp.toDateString());
            });
            status.current_timestamps = status.timestamps[0];
            status.data_timestamps[dataModelObject.getCurrentTimestampKey()] = {};
            return status.timestamps;
        });
        maxActivitySetPromise = getMaxActivityPromise.then(function (data) {
            status.statistic_values = data[0];
            status.temp_max = dataModelObject.getMaxActivity();
            return status.statistic_values;
        });
        squareDataPromise = squareDataPromise.then(function (data) {
            _.each(data.features, function (feature) {
                var cellId = feature.properties.cellId;
                status.squares_geometry[cellId] = feature.geometry;
            });
        });
        setDataTimestamps = function (data) {
            if (data) {
                status.data_timestamps[dataModelObject.getCurrentTimestampKey()] = data;
            }
            return data;
        };
        dataPromise = Q.all([timestampSetPromise, maxActivitySetPromise, squareDataPromise]).then(function () {
            return getSquaresData(status.current_timestamps).then(setDataTimestamps);
        });
        dataModelObject = {
            status: status,
            setStatus: function (key, value) {
                status[key] = value;
            },
            getTimestamps: getTimestampsPromise,
            getMaxActivityPromise: getMaxActivityPromise,
            getDataPromise: function () {
                return dataPromise;
            },
            getMaxActivity: function (type) {
                if (type === undefined) {
                    type = 'max';
                }
                var keys = _.filter(['sms_in', 'sms_out', 'call_in', 'call_out', 'internet'], function (elem) {
                    return status[elem];
                }),
                    values = _.map(keys, function (key) {return status.statistic_values[type + '_' + key]; });
                return _.max(values);
            },
            getActivityValues: function (value) {
                var max, minorOneValue0, minorOneValue1;
                if (status.low_scale) {
                    max = this.getMaxActivity('avg');
                } else {
                    max = this.getMaxActivity();
                }
                if (!value) {
                    value = status.activity_value_range;
                }
                if (status.statistic_values) {
                    minorOneValue0 = value[0] / status.activity_max_value;
                    minorOneValue1 = value[1] / status.activity_max_value;
                    return [max * minorOneValue0, max * minorOneValue1];
                }
                return [null, null];
            },
            getActivityRelativeValue: function (value) {
                var max, minorOneValue;
                if (status.low_scale) {
                    max = this.getMaxActivity('avg');
                } else {
                    max = this.getMaxActivity();
                }
                if (!value) {
                    value = status.activity_relative_value;
                }
                if (status.statistic_values) {
                    minorOneValue = value / status.activity_max_relative_value;
                    return max * minorOneValue;
                }
                return null;
            },
            update: function () {
                status.temp_max = this.getMaxActivity();
                return this.cacheData(status.current_timestamps).then(setDataTimestamps);
            },
            cacheData: function (timestamp) {
                var timestampKey = status.group_by + timestamp;
                if (timestamp && !status.data_timestamps[timestampKey] && !status.data_timestamps_cache_promise[timestampKey]) {
                    status.data_timestamps_cache_promise[timestampKey] =
                        this.getGetDataFunc(status.current_timestamps).then(function (data) {
                            status.data_timestamps_cache_promise[timestampKey] = undefined;
                            return data;
                        });
                    return status.data_timestamps_cache_promise[timestampKey];
                }
                return Q(undefined);
            },
            getGetDataFunc: function (timestamp) {
                if (status.group_by === 'hour') {
                    return getSquaresData(timestamp);
                }
                if (status.group_by === 'day') {
                    return getSquaresDataGroupByDay(timestamp);
                }
                return Q(undefined);
            },
            getAllCurrentData:  function () {
                return status.data_timestamps[this.getCurrentTimestampKey()];
            },
            getCurrentTimestampKey: function () {
                if (status.current_timestamps) {
                    return status.group_by + status.current_timestamps.toString();
                }
                return undefined;
            },
            getTimestampsArray: function (index) {
                var timestamps;
                if (status.group_by === 'hour') {
                    timestamps = status.timestamps;
                }
                if (status.group_by === 'day') {
                    timestamps = status.timestamps_days;
                }
                if (index !== undefined) {
                    return timestamps[index];
                }
                return timestamps;
            },
            isDataToShow: function (meanActivityValue, rangeActivityValues) {
                return meanActivityValue >= rangeActivityValues[0] && meanActivityValue <= rangeActivityValues[1];
            },
            getSquareAbsoluteValue: function (square) {
                var sum = 0, keyCount = 0;
                if (status.sms_in) {
                    sum += square.s_i;
                    keyCount += 1;
                }
                if (status.sms_out) {
                    sum += square.s_o;
                    keyCount += 1;
                }
                if (status.call_in) {
                    sum += square.c_i;
                    keyCount += 1;
                }
                if (status.call_out) {
                    sum += square.c_o;
                    keyCount += 1;
                }
                if (status.internet) {
                    sum += square['in'];
                    keyCount += 1;
                }
                return sum / keyCount;
            }
        };
        return dataModelObject;
    }
);