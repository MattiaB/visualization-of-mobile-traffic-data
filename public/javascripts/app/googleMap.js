/*global define:false, $:false, google:false, document:false */

define(
    ['jquery', 'underscore', 'app/datamodel', 'jquery_ui'],
    function ($, _, model) {
        "use strict";
        var createSearchBox, addDrawController, initializeMap, map, prevStatusMap = {},
            shape, shapeInfoWindow, polygons = {}, makePolygonColor, convertToGeoData, getColorGeneralMeanValue, getTypeOfColorFunc, getColorForInOutValue;
        createSearchBox = function () {
            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input'), searchBox, i, markers = [], bounds, image;
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            searchBox = new google.maps.places.SearchBox(input);

            // Listen for the event fired when the user selects an item from the
            // pick list. Retrieve the matching places for that item.
            google.maps.event.addListener(searchBox, 'places_changed', function () {
                var places = searchBox.getPlaces(), i, bounds = new google.maps.LatLngBounds();
                _.each(markers, function (marker) {
                    marker.setMap(null);
                });
                // For each place, get the icon, place name, and location.
                markers = _.map(places, function (place) {
                    var image = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };
                    bounds.extend(place.geometry.location);
                    return new google.maps.Marker({
                        map: map,
                        icon: image,
                        title: place.name,
                        position: place.geometry.location
                    });
                });
                map.fitBounds(bounds);
            });

            // Bias the SearchBox results towards places that are within the bounds of the
            // current map's viewport.
            google.maps.event.addListener(map, 'bounds_changed', function () {
                var bounds = map.getBounds();
                searchBox.setBounds(bounds);
            });

        };
        addDrawController = function () {
            var polyOptions = {
                strokeWeight: 0,
                fillOpacity: 0.45,
                editable: false,
                draggable: false
            }, drawingManager;
            // Creates a drawing manager attached to the map that allows the user to draw
            // markers, lines, and shapes.
            drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: null,
                drawingControlOptions: {
                    drawingModes: [google.maps.drawing.OverlayType.RECTANGLE],
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                rectangleOptions: polyOptions,
                map: map
            });

            google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
                if (event.type !== google.maps.drawing.OverlayType.MARKER) {
                    var contentString;
                    if (shape) {
                        shape.setMap(null);
                        shapeInfoWindow.setMap(null);
                    }
                    shape = event.overlay;
                    shape.type = event.type;
                    // Switch back to non-drawing mode after drawing a shape.
                    drawingManager.setDrawingMode(null);
                    shapeInfoWindow = new google.maps.InfoWindow();
                    var ne = shape.getBounds().getNorthEast();
                    var sw = shape.getBounds().getSouthWest();
                    var jsonData = {
                        northEastLat:  ne.lat(),
                        northEastLng:  ne.lng(),
                        southWestLat:  sw.lat(),
                        southWestLng:  sw.lng()
                    };
                    var idSmsCall = 'chart_div_annotationchart';
                    var idInternet = 'chart_div_annotationchartInternet';
                    $.getJSON('/getDataWithGeoFilter', jsonData, function (dataFromServer) {
                        var dataSmsCall = new google.visualization.DataTable(),
                            dataInternet = new google.visualization.DataTable();
                        contentString = '<ul id="mytabannotationchart" class="nav nav-tabs">' +
                            '<li class="active"><a href="#home" data-toggle="tab">SMS and Call</a></li>' +
                            '<li ><a href="#profile" data-toggle="tab">Internet</a></li>' +
                            '</ul>' +
                            '<div class="tab-content"><div class="tab-pane active" id="home">' +
                            '<div class="infobox"  id="' + idSmsCall + '"  style=" width: 650px; height: 450px;"></div>' +
                            '</div>' +
                            '<div class="tab-pane" id="profile">' +
                            '<div class="infobox"  id="' + idInternet + '"  style=" width: 650px; height: 450px;"></div>' +
                            '</div>' +
                            '</div>';
                        shapeInfoWindow.setContent(contentString);
                        dataSmsCall.addColumn('date', 'Date');
                        dataSmsCall.addColumn('number', 'SMS in');
                        dataSmsCall.addColumn('number', 'SMS out');
                        dataSmsCall.addColumn('number', 'Call in');
                        dataSmsCall.addColumn('number', 'Call out');
                        dataInternet.addColumn('date', 'Date');
                        dataInternet.addColumn('number', 'Internet');

                        dataSmsCall.addRows(_.map(dataFromServer, function (elem) {
                            return [new Date(elem.time), elem.avg_sms_in, elem.avg_sms_out, elem.avg_call_in, elem.avg_call_out];
                        }));

                        var chart = new google.visualization.AnnotationChart(document.getElementById(idSmsCall));

                        var options = {
                            displayZoomButtons: false,
                            dateFormat: 'HH:mm ccc MMMM dd, yyyy'
                        };

                        chart.draw(dataSmsCall, options);
                        var chart2;
                        $('#mytabannotationchart a').click(function (e) {
                            e.preventDefault();
                            $(this).tab('show');
                            if (chart2 === undefined) {
                                dataInternet.addRows(_.map(dataFromServer, function (elem) {
                                    return [new Date(elem.time), elem.avg_internet];
                                }));
                                chart2 = new google.visualization.AnnotationChart(document.getElementById(idInternet));
                                chart2.draw(dataInternet, {
                                    displayZoomButtons: false,
                                    dateFormat: 'HH:mm ccc MMMM dd, yyyy'
                                });
                            }
                        });

                    });


                    // Set the info window's content and position.
                    shapeInfoWindow.setContent('<div class="infobox waitbox" id="' + idSmsCall + '"  style="max-width: 800px;width: 650px; height: 450px;">Wait few second while the chart is loading ...</div>');
                    shapeInfoWindow.setPosition(event.overlay.getBounds().getCenter());
                    shapeInfoWindow.open(map);
                    google.maps.event.addListener(shapeInfoWindow, 'closeclick', function() {
                        shape.setMap(null);
                        shapeInfoWindow.setMap(null);
                    });
                    // Add an event listener that selects the newly-drawn shape when the user
                    // mouses down on it.

                    // Add an event listener on the rectangle.
                    google.maps.event.addListener(shape, 'bounds_changed', function (event) {
                        shapeInfoWindow.setPosition(shape.getBounds().getCenter());
                    });

                }
            });
        };
        makePolygonColor = function (value) {
            var r = 0,
                g = 0,
                b = 255;
            value = Math.abs(value);
            if (value > 1) {
                value = 1;
            }
            if (value < 0.5) {
                r = 255;
                g = Math.floor(60 * (1 - value)) + 195;
                b = 0;
            } else {
                r = 0;
                g = Math.abs(Math.floor(280 *  (1 - value)));
                b = 220;
            }
            return ["rgb(" + r + "," + g + "," + b + ")", 0.5];
        };
        convertToGeoData = function (geoJsonData) {
            return _.map(geoJsonData, function (elem) {
                return new google.maps.LatLng(elem[1], elem[0]);
            });
        };
        initializeMap = function () {
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 12,
                center: new google.maps.LatLng(45.46480269430631, 9.190819580078141),
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR
                },
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL
                },
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        };
        getColorGeneralMeanValue = function (polygon, square, rangeActivityValues, activityRelativeValue) {
            var squareAbsoluteValue = model.getSquareAbsoluteValue(square);
            if (!model.isDataToShow(squareAbsoluteValue, rangeActivityValues)) {
                if (polygon) {
                    polygon.setMap(null);
                }
                return false;
            }
            return makePolygonColor(squareAbsoluteValue / activityRelativeValue);
        };
        getColorForInOutValue = function (polygon, square, rangeActivityValues, activityRelativeValue) {
            var inValue, outValue, keys, r, g, b, showValue;
            if (model.status.sms_in) {
                keys = ['s_i', 's_o'];
            } else if (model.status.call_in) {
                keys = ['c_i', 'c_o'];
            }
            inValue = square[keys[0]];
            outValue = square[keys[1]];
            if (!(model.isDataToShow(inValue, rangeActivityValues) || model.isDataToShow(outValue, rangeActivityValues))) {
                if (polygon) {
                    polygon.setMap(null);
                }
                return false;
            }
            if (inValue > outValue) {
                showValue = inValue / activityRelativeValue;
                if (showValue > 1) {
                    showValue = 1;
                }
                r = 0;
                g = Math.abs(Math.floor(100 *  (1 - showValue)));
                b = 255;
            } else {
                showValue = outValue / activityRelativeValue;
                if (showValue > 1) {
                    showValue = 1;
                }
                r = 255;
                g = 0;
                b = Math.abs(Math.floor(50 *  (1 - showValue)));
            }
            return ["rgb(" + r + "," + g + "," + b + ")", showValue * 0.5];
        };
        getTypeOfColorFunc = function () {
            if (((model.status.sms_in && model.status.sms_out) ||
                (model.status.call_in && model.status.call_out)) && !((model.status.call_in || model.status.call_out) && (model.status.sms_in || model.status.sms_out))) {
                return 2;
            }
            return 1;
        };
        return {
            initializeMap: function () {
                initializeMap();
                createSearchBox();
                addDrawController();
            },
            deleteShape: function () {
                if (shape) {
                    shape.setMap(null);
                    shapeInfoWindow.setMap(null);
                }
                shape = null;
                shapeInfoWindow = null;
            },
            updateView : function () {
                if (model.getAllCurrentData() === undefined) {
                    return;
                }
                var allData = model.getAllCurrentData(),
                    dataKey = model.getCurrentTimestampKey(),
                    activityRelativeValue = model.getActivityRelativeValue(),
                    rangeActivityValues = model.getActivityValues(),
                    generateColorFunc,
                    typeOfColorFunc = getTypeOfColorFunc(),
                    isEqualToPrev,
                    actualStatus;
                if (typeOfColorFunc === undefined || !allData || !(allData.length > 0)) {
                    return;
                }
                actualStatus = {
                    dataKey: dataKey,
                    activityRelativeValue: activityRelativeValue,
                    rangeActivityValues: rangeActivityValues,
                    typeOfColorFunc: typeOfColorFunc
                };
                isEqualToPrev = _.isEqual(actualStatus, prevStatusMap);
                if (isEqualToPrev) {
                    return;
                }
                prevStatusMap = actualStatus;

                if (typeOfColorFunc === 1) {
                    generateColorFunc = getColorGeneralMeanValue;
                }
                if (typeOfColorFunc === 2) {
                    generateColorFunc = getColorForInOutValue;
                }
                _.each(allData, function (square) {
                    var polygon = polygons[square.id], fillColor, paths;
                    fillColor = generateColorFunc(polygon, square, rangeActivityValues, activityRelativeValue);
                    if (fillColor === false) {
                        return;
                    }
                    if (polygon) {
                        polygon.setOptions({
                            fillColor: fillColor[0],
                            fillOpacity: fillColor[1]
                        });
                        polygon.setMap(map);
                    } else {
                        paths = model.status.squares_geometry[square.id].google_maps_path;
                        if (paths === undefined) {
                            paths = convertToGeoData(model.status.squares_geometry[square.id].coordinates[0]);
                            model.status.squares_geometry[square.id].google_maps_path = paths;
                        }
                        polygon = new google.maps.Polygon({
                            paths: paths,
                            strokeWeight: 0,
                            fillColor: fillColor[0],
                            fillOpacity: fillColor[1]
                        });
                        polygon.setMap(map);
                        polygons[square.id] = polygon;
                    }
                });
            }
        };
    }
);