/*global requirejs:false, require:false, _:false, $:false, console:false */


requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: '/javascripts/',
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        app: 'app',
        jquery: ['http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.0/jquery.min'],
        underscore: 'http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min',
        //doT: 'doT',
        bootstrap: ['http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min'],
        jquery_ui: ['jquery-ui-1.10.3.custom.min'],
        q: ['http://cdnjs.cloudflare.com/ajax/libs/q.js/1.0.0/q.min'],
        moment: ['http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.5.1/moment.min'],
        dot: ['doT.min']
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        },
        "jquery_ui": {
            deps: ["jquery", "bootstrap"]
        },
        'underscore': {
            exports: '_'
        }
    }
});
requirejs.onError = function (err) {
    "use strict";
    console.log(err.requireType);
    if (err.requireType === 'timeout') {
        console.log('modules: ' + err.requireModules);
    }

    throw err;
};
require(['app/userInterface', 'app/googleMap', 'jquery', 'bootstrap', 'jquery_ui'],
    function (userInterface, googleMaps) {
        "use strict";
        googleMaps.initializeMap();
        userInterface.initializeEvent();
    });