// Adding 500 Data Points

$(function() {
    $( "#time-slider" ).slider();
    $("#time-play").button({
        text: false,
        icons: {
            primary: "ui-icon-play"
        }
    });
    $('.button-info').button({
        text: false,
        icons: {
            primary: "ui-icon-info"
        }
    });
    $('#activity-range-slider').slider({range: true, values: [ 30, 60 ]});
    $( "#relative-activity-slider" ).slider();

    $('#activity-sms-type-selector').buttonset();
    $('#activity-call-type-selector').buttonset();
    $( "#activity-internet-type-button" ).buttonset();
    $('#activity-log-scale-button').buttonset()
});
var map, pointarray, heatmap, squarePoint, squareData, timestamps, getDataToShow,
    getDataToShowKey = 'logmax', type = 'smsCall', polygons = {}, generalsMeans = {};

function createSearchBox(map) {
    // Create the search box and link it to the UI element.
    markers = [];
    var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var searchBox = new google.maps.places.SearchBox(
        /** @type {HTMLInputElement} */(input));

    // Listen for the event fired when the user selects an item from the
    // pick list. Retrieve the matching places for that item.
    google.maps.event.addListener(searchBox, 'places_changed', function() {
        var places = searchBox.getPlaces();

        for (var i = 0, marker; marker = markers[i]; i++) {
            marker.setMap(null);
        }

        // For each place, get the icon, place name, and location.
        markers = [];
        var bounds = new google.maps.LatLngBounds();
        for (var i = 0, place; place = places[i]; i++) {
            var image = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            var marker = new google.maps.Marker({
                map: map,
                icon: image,
                title: place.name,
                position: place.geometry.location
            });

            markers.push(marker);

            bounds.extend(place.geometry.location);
        }

        map.fitBounds(bounds);
    });

    // Bias the SearchBox results towards places that are within the bounds of the
    // current map's viewport.
    google.maps.event.addListener(map, 'bounds_changed', function() {
        var bounds = map.getBounds();
        searchBox.setBounds(bounds);
    });

}
function makeColor(value) {
    var r = 0,
        g = Math.floor(255 * Math.abs(value - 1)),
        b = 255;
    return "rgb(" + r + "," + g + "," + b + ")";
}
function loadJsonData(url) {
    var xmlhttp;
    var deferred = Q.defer();

    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            deferred.resolve(JSON.parse(xmlhttp.responseText));
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
    return deferred.promise;
}
var convertGeoData = function (geoData) {
    var result = [];
    for (var i = 0; i < geoData.length; i++) {
        var coords = geoData[i];
        result.push(new google.maps.LatLng(coords[1], coords[0]))
    }
    return result;
};
var modifyRangeValue = function (value) {
    var date = new Date(timestamps[value]);
    $('#textdate').text(date.toUTCString());
};
var typeFunction = {
    'internet' : function (squareObject) {
        return squareObject.internet;
    },
    'smsCall' : function (squareObject) {
        return squareObject.mean;
    }
};
var getDataToShowFunctions = {
    'mean': function (squareData) {
        var sum = _.reduce(squareData,function (memo, num) {
            return memo +  typeFunction[type](num);
        }, 0);
        var mean = sum / squareData.length;

        return function (squareObject) {
            return typeFunction[type](squareObject) / mean * 0.5
        }
    },
    'max': function (squareData) {
        var max = _.max(squareData, function (value) {
            return typeFunction[type](value)
        });
        max = typeFunction[type](max);
        return function (squareObject) {
            return typeFunction[type](squareObject) / max
        }
    },
    'logmax': function (squareData) {
        var max = _.max(squareData, function (value) {
            return Math.log(typeFunction[type](value) + 1)
        });
        max = Math.log(typeFunction[type](max) + 1);
        return function (squareObject) {
            return Math.log(typeFunction[type](squareObject) + 1) / max
        }
    },
    'general_mean' : function () {
        return function (squareObject) {
            //return (typeFunction[type](squareObject) / generalsMeans[squareObject.id][type]) - 1
            return (typeFunction[type](squareObject) / generalsMeans[squareObject.id][type]) * 0.5
        }
    }
};
var resultsCallback = function (squareData) {
    getDataToShow = getDataToShowFunctions[getDataToShowKey](squareData);
    for (var i = 0; i < squareData.length; i++) {
        if (polygons[squareData[i].id]) {
            polygons[squareData[i].id].setOptions({
                fillColor: makeColor(getDataToShow(squareData[i]))
            });
        } else {
            var polygon = new google.maps.Polygon({
                paths: convertGeoData(squarePoint[squareData[i].id].coordinates[0]),
                strokeWeight: 0,
                fillColor: makeColor(getDataToShow(squareData[i])),
                fillOpacity: 0.35
            });
            polygon.setMap(map);
            polygons[squareData[i].id] = polygon;
        }
    }
};
var updateMapWithNewTimeStamp = function (timestamp) {
    var date = new Date(timestamp);
    $('#textdate').text(date.toUTCString());
    $('#updating').text('In Update, Wait');
    return loadJsonData("http://localhost:3000/getGeneralMeans?hourfield="
        + date.getHours() + '&dayfield=' + (date.getDay() + 1)).then(function (data) {
            _.each(data, function (value) {
                generalsMeans[value.id] = {
                    'smsCall': value.mean_sms_call,
                    'internet': value.mean_internet
                };
            });
            return loadJsonData("http://localhost:3000/getData?timestamp=" + timestamp);
        }).then(function (data) {
            squareData = data;
            resultsCallback(data);
            $('#updating').text('');
        });
};
function addDrawControl(map) {
    var polyOptions = {
        strokeWeight: 0,
        fillOpacity: 0.45,
        editable: true,
        draggable: true
    };
    // Creates a drawing manager attached to the map that allows the user to draw
    // markers, lines, and shapes.
    drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: null,
        drawingControlOptions: {
            drawingModes: [google.maps.drawing.OverlayType.RECTANGLE],
            position: google.maps.ControlPosition.TOP_CENTER
        },
        rectangleOptions: polyOptions,
        map: map
    });
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
        if (e.type != google.maps.drawing.OverlayType.MARKER) {
            // Switch back to non-drawing mode after drawing a shape.
            drawingManager.setDrawingMode(null);
            infoWindow = new google.maps.InfoWindow();
            var ne = e.overlay.getBounds().getNorthEast();
            var sw = e.overlay.getBounds().getSouthWest();

            var contentString = "Qui comparirà un grafico con " +
                "l'andamento medio nel tempo dell'attività telefonica selezionata</br> (Area occupata)" +
                "</br></br></br></br></br></br></br></br></br></br></br></br></br></br>";

            // Set the info window's content and position.
            infoWindow.setContent(contentString);
            infoWindow.setPosition(e.overlay.getBounds().getCenter());

            infoWindow.open(map);
            // Add an event listener that selects the newly-drawn shape when the user
            // mouses down on it.
            var newShape = e.overlay;
            newShape.type = e.type;
            google.maps.event.addListener(newShape, 'click', function() {
                setSelection(newShape);
            });
            setSelection(newShape);
        }
    });
}
function initialize() {
    var mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng( 45.46480269430631, 9.190819580078141),
        mapTypeControl: true,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL
        },
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);

    createSearchBox(map);
    addDrawControl(map)
    loadJsonData("http://localhost:3000/getCellData").then(function (data) {
        squarePoint = {};
        for (var i = 0; i < data.features.length; i++) {
            var cellId = data.features[i].properties.cellId;
            squarePoint[cellId] = data.features[i].geometry
        }
        return loadJsonData("http://localhost:3000/getTimestamps");
    }).then(function (data) {
            timestamps = _.map(data, function (timestamp) {
                return parseInt(timestamp.timestamp, 10);
            });
            timestamps = _.sortBy(timestamps);
            $("#rangeinput").attr('max', timestamps.length - 1);
            $("#rangeinput").attr('value', 0);
            modifyRangeValue($('#rangeinput').val());
            return loadJsonData("http://localhost:3000/getData?timestamp=" + timestamps[0]);
        }).then(function (data) {
            squareData = data;
            resultsCallback(data);
            var dateTimestamp = new Date(timestamps[0]);
            return loadJsonData("http://localhost:3000/getGeneralMeans?hourfield="
                + dateTimestamp.getHours()  + '&dayfield=' + (dateTimestamp.getDay() + 1));
        }).then(function (data) {
            _.each(data, function (value) {
                generalsMeans[value.id] = {
                    'smsCall': value.mean_sms_call,
                    'internet': value.mean_internet
                };
            });
        });
    $('#updateMap').click(function () {
        updateMapWithNewTimeStamp(timestamps[$("#rangeinput").val()]).done();
    });
    $("#rangeinput").on("input change",function(){
        modifyRangeValue($(this).val())
    });
    $('#max').click(function () {
        getDataToShowKey = 'max';
    });
    $('#logmax').click(function () {
        getDataToShowKey = 'logmax';
    });
    $('#mean').click(function () {
        getDataToShowKey = 'mean';
    });
    $('#general_mean').click(function () {
        getDataToShowKey = 'general_mean';
    });
    $('#smsCall').click(function () {
        type = 'smsCall'
    });
    $('#internet').click(function () {
        type = 'internet'
    });
    var run = false;
    $('#simulation').click(function () {
        if (!run) {
            run = true;
            var i = parseInt($("#rangeinput").val(), 10);
            var simulation = function () {
                updateMapWithNewTimeStamp(timestamps[i]).then(function () {
                    i += 1;
                    if (run && i < timestamps.length) {
                        setTimeout(simulation, 500)
                    }
                }, function (err) {
                    console.log(err)
                });
            };
            simulation();
        } else {
            run = false
        }
    });
}
google.maps.event.addDomListener(window, 'load', initialize);